import sys
import os
import argparse

from classes.contact_sheet import ContactSheet
from glob import glob


def main(dir):
    file_string = os.path.join(dir, '*.jpg')
    cs_name = os.path.join(dir, 'contact_sheet.png')
    fnames = glob(file_string)
    cs = ContactSheet.make_contact_sheet(fnames, (200, 200), 4, (5, 5, 5, 5), 2)
    cs.save(cs_name)


if __name__ == '__main__':
    dir = sys.argv[1]
    if not os.path.isdir(dir):
        raise Exception('The directory does not exist: [{}]'.format(dir))
    main(dir)
