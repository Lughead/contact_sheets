import math
import logging

from PIL import Image

__author__ = 'jedmitten@alumni.cmu.edu'


log = logging.getLogger(__name__)


def is_image(fpath):
    try:
        with Image.open(fpath):
            return True
    except Exception:
        return False


def get_max_dimensions(images):
    """
    Return the maximum width found in the image set
    """
    sizes = [Image.open(i).size for i in images if is_image(i)]
    widths = [s[0] for s in sizes]
    heights = [s[1] for s in sizes]
    return max(widths), max(heights)


def get_scale_from_max(images, max_w, max_h):
    images_max_w, images_max_h = get_max_dimensions(images)
    w_scale, h_scale = 1.0, 1.0

    if max_w < images_max_w:
        w_scale = max_w / images_max_w

    if max_h < images_max_h:
        h_scale = max_h / images_max_h

    return min((w_scale, h_scale))


class ImageRow:
    def __init__(self, images):
        self.images = images
        self.row_width, self.row_height = self.get_row_dimensions()

    def get_row_dimensions(self):
        height = 0
        width = 0
        for image in self.images:
            im_width, im_height = image.size
            if im_height > height:
                height = im_height

            if im_width > width:
                width = im_width

        return width, height


class ContactSheet(object):
    MAX_DIM = 65500
    def __init__(self, image_list=list(), max_width=150, max_height=150, num_cols=4, margins=(5, 5, 5, 5), padding=2):
        self.max_width = max_width
        self.max_height = max_height
        self.num_columns = num_cols
        valid_images = [i for i in image_list if is_image(i)]
        self.image_list = self.create_rows(valid_images)
        self.margins = margins
        assert hasattr(self.margins, '__iter__')
        self.padding = padding

    def create_rows(self, image_list):
        num_rows = math.ceil(len(image_list) / self.num_columns)
        image_rows = list()
        for row_idx in range(num_rows):
            for col_idx in range(self.num_columns):
                idx = row_idx * self.num_columns + col_idx
                image_rows.append(image_list[idx])
        return image_list

    # from https://code.google.com/p/pistol/source/browse/trunk/Pistol/make-contact-sheet.py
    @staticmethod
    def make_contact_sheet(fnames, maximums, num_cols, mdims, padding):
        """
        Read images from a list and create an image out of thumbnails
        :param fnames: A list of names of the image files
        :param maximums: A tuple with max width and max height
        :param num_cols: The number of columns to display
        :param mdims: A tuple of margin dimensions
        :param padding: The padding between images in pixels
        :return: a PIL image object.
        """
        # Read in all images and resize appropriately
        num_rows = math.ceil(len(fnames) / num_cols)

        margin_l, margin_t, margin_r, margin_b = mdims
        margin_w = margin_l + margin_r
        scale = get_scale_from_max(fnames, maximums[0], maximums[1])

        thumbnails = list()
        for image in fnames:
            if not is_image(image):
                continue
            im = Image.open(image)
            orig_width, orig_height = im.size
            new_width = int(orig_width * scale)
            new_height = int(orig_height * scale)
            try:
                thumbnails.append(Image.open(image).resize((new_width, new_height)))
            except OSError:
                log.debug('Error in image: [{}]. Skipping...'.format(image))
                continue

        img_rows = list()
        canvas_height = 0
        for row_idx in range(num_rows):
            low_idx = row_idx * num_cols
            high_idx = low_idx + num_cols - 1
            if high_idx >= len(fnames):
                high_idx = len(fnames)
            img_row = ImageRow(images=thumbnails[low_idx:high_idx])
            canvas_height += img_row.row_height
            img_rows.append(img_row)

        max_row_width, max_row_height = get_max_row_dimensions(img_rows)
        # Create the new image. The background doesn't have to be white
        bg_color = (255, 255, 255)  # white RGB
        padded_cs_w = (max_row_width + padding) * (num_cols - 1)
        canvas_width = padded_cs_w + margin_w
        if canvas_width > ContactSheet.MAX_DIM:
            canvas_width = ContactSheet.MAX_DIM
        canvas_height += (num_rows -1) * padding
        if canvas_height > ContactSheet.MAX_DIM:
            canvas_height = ContactSheet.MAX_DIM  # PIL max dimension
        cs_canvas = Image.new('RGB', (canvas_width, canvas_height), bg_color)
        # Calculate the size of the output image, based on the
        #  photo thumb sizes, margins, and padding
        # Insert each thumb:
        for row_idx in range(num_rows):
            low_idx = row_idx * num_cols
            high_idx = low_idx + num_cols - 1
            if high_idx >= len(fnames):
                high_idx = len(fnames)
            img_row = ImageRow(images=thumbnails[low_idx:high_idx])
            pt_left = margin_l
            pt_top = row_idx * max_row_height + margin_t

            for col_idx in range(len(img_row.images)):
                img = img_row.images[col_idx]
                img_width, img_height = img.size
                pt_right = pt_left + img_width + padding
                # pt_bot = pt_top + row_height + padding
                box_thumbnail = (pt_left, pt_top)
                cs_canvas.paste(img, box_thumbnail)
                pt_left = pt_right
        return cs_canvas


def get_max_row_dimensions(rows):
    max_w = 0
    max_h = 0
    for row in rows:
        width = row.row_width
        height = row.row_height

        if max_w < width:
            max_w = width

        if max_h < height:
            max_h = height

    return max_w, max_h

