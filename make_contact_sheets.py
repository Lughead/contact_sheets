import logging
import argparse
import os
import glob
import re
from classes.contact_sheet import ContactSheet

__author__ = 'jedmitten@alumni.cmu.edu'


VALID_DIRS = ['reviewed']


def create_cs_name(indir):
    _, indir = os.path.splitdrive(indir)
    indir = indir.replace("\\", "_").replace(".", "_")
    return indir + 'cs.jpg'


def get_domain_and_index(cs_fname):
    cs_re = re.compile('^.*_(\w+)_(\w+)_(\d+)_.*\.jpg$')
    m = cs_re.match(cs_fname)
    fn_parts = list()
    if m:
        fn_parts = m.groups()
    return fn_parts


def get_existing_contact_sheets(dirbase):
    existing_cs = set([])
    for root, dirs, files in os.walk(dirbase):
        if files:
            for file in files:
                # break out the domain and gallery_id
                _, filename = os.path.split(file)
                fn_parts = get_domain_and_index(filename)
                existing_cs.add(fn_parts)
    return existing_cs


def main(options):
    if not os.path.isdir('logs'):
        os.mkdir('logs')  # for local logging

    loglevel = logging.INFO
    if options.verbose:
        loglevel = logging.DEBUG
    logformat = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=loglevel, format=logformat)
    log = logging.getLogger("contact_sheets")
    fh = logging.FileHandler('logs/contact_sheets.log')
    fh.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter(logformat)
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    log.addHandler(fh)
    log.addHandler(ch)

    logging.info('Starting contact sheet process.')

    if not os.path.isdir(options.base_directory):
        raise ValueError('The directory is invalid: [{}]'.format(options.base_directory))

    if not os.path.isdir(options.output_directory):
        os.makedirs(os.path.abspath(options.output_directory))

    contact_sheet_list = get_existing_contact_sheets(dirbase=options.output_directory)
    for root, _, file_list in os.walk(options.base_directory):
        if file_list:
            cs_save_name = create_cs_name(root)
            cs_parts = get_domain_and_index(cs_save_name)
            if cs_parts not in contact_sheet_list:
                cs_fp = os.path.join(options.output_directory, cs_save_name)
                logging.info('Creating contact sheet for [{}] at [{}]'.format(root, cs_fp))
                cs = ContactSheet.make_contact_sheet(fnames=[os.path.join(root, f) for f in file_list],
                                                     maximums=(150, 150), num_cols=5, mdims=(5, 5, 5, 5), padding=4)
                cs.save(cs_fp)
            else:
                logging.debug('Skipping contact sheet for [{}]'.format(dir))

    logging.info('Finished contact sheet process.')


def get_options():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--base-directory', help='The directory from which to spider and make contact sheets',
                        required=True)
    parser.add_argument('-o', '--output-directory', help='The directory to save the contact sheets', required=True)
    parser.add_argument('-v', '--verbose', action='store_true')
    return parser.parse_args()


if __name__ == '__main__':
    options = get_options()
    main(options)
